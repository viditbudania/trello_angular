import { Injectable } from '@angular/core';
import { HttpClient,HttpParams} from '@angular/common/http'
import { IBoardList} from './boardList'
import { ICardList } from './cardList'
import { Observable, from } from 'rxjs';



let key:string = "5faed5e4da997405925813595071a24a";
let token : string = "0e4552d74e22a4b469be62aed5ce3c6bebb94cd20add47f16587fc0ff37ccc6e";
let boardId: string = "9K1lhWei";

@Injectable({
  providedIn: 'root'
})

export class BoardService {

  private url: string = `https://api.trello.com/1/boards/${boardId}/lists?cards=none&card_fields=all&filter=open&fields=all&key=${key}&token=${token}` ;

  constructor( private http : HttpClient) { }

  getBoard (): Observable <IBoardList[]>{
    return this.http.get<IBoardList[]>(this.url);
  }

  private url_cardList: string =`https://api.trello.com/1/boards/${boardId}/cards/?key=${key}&token=${token}`
  getCards (): Observable<ICardList[]> {
    return this.http.get<ICardList[]>(this.url_cardList)
  }

  deleteCard (id) {
    return this.http.delete(`https://api.trello.com/1/cards/${id}?key=5faed5e4da997405925813595071a24a&token=0e4552d74e22a4b469be62aed5ce3c6bebb94cd20add47f16587fc0ff37ccc6e`)
  }

addList(name){
  let params = new HttpParams().set('name', null).set('idBoard', null)
  return this.http.post(`https://api.trello.com/1/boards/${boardId}/lists?name=${name}&pos=top&key=${key}&token=${token}`, {params})
}

addCard(input,listId){
  let params = new HttpParams().set('name',null)
  return this.http.post(`https://api.trello.com/1/cards?name=${input}&pos=bottom&idList=${listId}&keepFromSource=all&key=${key}&token=${token}`,{params})
}

}
