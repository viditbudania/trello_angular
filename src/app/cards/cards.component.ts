import { Component, OnInit, Input } from '@angular/core';
import { CheckListService } from '../check-list.service'
import { Location } from '@angular/common'
import { ActivatedRoute } from '@angular/router'


@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  @Input() childMessage: string;@Input() name : string;
public cardId = this.route.snapshot.paramMap.get('cardId')
public cardName = this.route.snapshot.paramMap.get('cardName')
public newCheckListName=""
public newCheckItemName=""
public checkList=[]
  constructor(
    private checkListService: CheckListService,
    private location : Location,
    private route : ActivatedRoute
    ) { }

    deleteCheckList(data){
      this.ngOnInit()
      console.log(data.id)
      console.log("hi")
      this.checkListService.deleteCheckList(data.id).subscribe()
      this.ngOnInit();
    }

    addCheckList(){
      this.ngOnInit()
      console.log(this.childMessage,this.newCheckListName)
      this.checkListService.addCheckList(this.childMessage,this.newCheckListName).subscribe()
      this.ngOnInit()
    }

    addCheckItem(checkList){
      this.ngOnInit()
      console.log(checkList.id, this.newCheckItemName)
      this.checkListService.addCheckItem(checkList.id,this.newCheckItemName).subscribe()
      this.ngOnInit()
    }
 
    deleteCheckListItem(checklist,checkitem){
      this.ngOnInit()
      // console.log("hi")
      // console.log(checklist.id)
      // console.log(checkitem.id)
      this.checkListService.deleteCheckListItem(checklist.id,checkitem.id).subscribe()
      this.ngOnInit()
    }

  ngOnInit() {
    // console.log("heyyy")
    //  console.log(this.childMessage)
    this.checkListService.getCheckList(this.childMessage)
    .subscribe(data=>{
      console.log(data)
      this.checkList=data
    })

   

  }

}
