import { Injectable } from '@angular/core';
import { HttpClient, HttpParams} from '@angular/common/http'
import { Observable, from } from 'rxjs';
import { ICheckList } from './checkList'


let key:string = "5faed5e4da997405925813595071a24a";
let token : string = "0e4552d74e22a4b469be62aed5ce3c6bebb94cd20add47f16587fc0ff37ccc6e";
let boardId: string = "9K1lhWei";


@Injectable({
  providedIn: 'root'
})
export class CheckListService {

  constructor( private http : HttpClient) { }


  getCheckList(id) : Observable<ICheckList[]> {
 return this.http.get<ICheckList[]>(`https://api.trello.com/1/cards/${id}/checklists?key=${key}&token=${token}`)
  }
deleteCheckList(id){
  return this.http.delete(`https://api.trello.com/1/checklists/${id}?key=${key}&token=${token}`)
}
deleteCheckListItem(idCheckList,idCheckItem) {
  return this.http.delete(`https://api.trello.com/1/checklists/${idCheckList}/checkItems/${idCheckItem}?key=${key}&token=${token}`)
}

addCheckList(id, name){
  let params = new HttpParams().set('name',null)
  return this.http.post(`https://api.trello.com/1/checklists?idCard=${id}&name=${name}&pos=bottom&key=${key}&token=${token}`,null)
}
addCheckItem(id,name){
  let params = new HttpParams().set('name',null)
return this.http.post(`https://api.trello.com/1/checklists/${id}/checkItems?name=${name}&pos=bottom&checked=false&key=${key}&token=${token}`, null)
}

}
