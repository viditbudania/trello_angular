import { Component, OnInit } from '@angular/core';
import { BoardService } from '../board.service';
// import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
public boardList =[];
public cardList =[];
public newListName = "";
public newCardName = "";
public listId = "";
public status : boolean;
public parentMessage = ""
public nameOfCard = ""
  constructor(private boardService : BoardService) { }

addCard(id){
console.log(id)
console.log(this.newCardName)
this.ngOnInit()
this.boardService.addCard(this.newCardName,id).subscribe()
this.ngOnInit()
}

  delete(event){
    // console.log(event.id)
    this.ngOnInit()
    this.boardService.deleteCard(event.id).subscribe()
    this.ngOnInit()
  }

addList() {
  this.boardService.addList(this.newListName)
  .subscribe(  )
  this.boardList.push(this.newListName)

}
close() {
  // console.log(this.status)
  this.status=false;
}
  ngOnInit() {
    this.boardService.getBoard()
      .subscribe(data =>{ 
        // console.log(data)
        this.boardList = data});
      
    this.boardService.getCards()
      .subscribe(data =>{ 
        // console.log(data) 
        this.cardList = data});
  }
  
  showPopUp(IdCard){
    // console.log("hi")
    this.parentMessage = IdCard.id; 
    this.nameOfCard = IdCard.name;
    // console.log(this.parentMessage)
    this.status=!this.status
    // console.log(this.status)
  }
}
